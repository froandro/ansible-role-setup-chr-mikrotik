Описание
=========

Ansible-роль предназначена для автоматической настройки программного роутера CHR от Mikrotik.
Содержит возможность автоматической настройки:
- l2tp-сервера и создание l2tp-туннеля с базовой защитой ipsec;
- firewall-правил;
- ospf-роутинга;
- отключение сервисов: ftp,www,telnet,api;
- отключение сервисных портов: ftp,tftp,h323,sip,pptp


Предварительные требования
------------

- настроен удаленный доступ на CHR (по ssh или по паролю);
- на localhost предварительно должны быть установлены:
  - paramiko (```pip install paramiko```);
  - community.routeros (```ansible-galaxy collection install community.routeros```)


Используемые переменные
--------------
В файле `defaults/main.yml` замените значения, содержащие `your ..`, на собственные. 

```
WAN_ip: your_wan_ip
white_ip: your_white_ip_or_url 
primary_ntp: your_ip_ntp_server 
secondary_ntp: your_ip_second_ntp_server 

l2tp_interface_name: l2tp-in1
l2tp_user: your_name_l2tp_user
l2tp_pass: your_l2tp_user_password
ipsec_pass: your_l2tp_ipsec_password
l2tp_local: your_l2tp_ip_local
l2tp_remote: your_l2tp_ip_remote
lan_vpn: your_l2tp_network
lan_lan: your_lan_network

ospf_router_id: your_ospf_router_id
src_address_list: lan
interface_list: LAN
```

Зависимости
------------

CHR - version 6.48.6


Пример Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
- hosts: chr

  pre_tasks:
    - name: Install paramiko python package
      ansible.builtin.pip:
        name: paramiko
      delegate_to: 127.0.0.1

  roles:
    - ansible_role_setup_chr

```

Лицензия
-------

BSD

Автор
------------------
AlZa 2022
